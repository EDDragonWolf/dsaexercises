/*
 * Copyright 2021 Vitalius Parubochyi, QtTeam
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NUMBERGENERATOR_H
#define NUMBERGENERATOR_H

#include <algorithm>
#include <vector>
#include <random>
#include <chrono>

namespace NumberGenerator
{

/*!
 * \brief Creates vector of unique random integers in the range [0, \a count).
 *
 * \param count The number of the integers, which will be generated.
 * \return The vector of unique random integers in the range [0, \a count).
 *
 * Usage example:
 *
 * #include "numbergenerator.h"
 *
 * std::vector<int> v = NumberGenerator::generate(10000);
 */
std::vector<int> generate(size_t count)
{
    std::vector<int> v;
    v.reserve(count);
    for (size_t i = 0; i < count; ++i)
    {
        v.push_back(static_cast<int>(i));
    }
    auto seed = static_cast<unsigned int>(
                std::chrono::system_clock::now().time_since_epoch().count());
    std::shuffle(v.begin(), v.end(), std::default_random_engine(seed));
    return v;
}

}

#endif // NUMBERGENERATOR_H
