#include "linkedlist.h"
#include <utility>

LinkedList::LinkedList() : mData{nullptr}, mSize{0}
{
}

LinkedList::LinkedList(const LinkedList &list)
{
    mSize = list.mSize;
    mData = nullptr;
    Node *current = nullptr;
    for (const Node *it = list.mData; it != nullptr; it = it->next)
    {
        if (!mData)
        {
            mData = new Node{it->value, nullptr};
            current = mData;
        }
        else
        {
            current->next = new Node{it->value, nullptr};
            current = current->next;
        }
    }
}

LinkedList::LinkedList(LinkedList &&list)
{
    mData = std::exchange(list.mData, nullptr);
    mSize = std::exchange(list.mSize, 0);
}

LinkedList::~LinkedList()
{
    Node *current = mData;
    while (current)
    {
        Node *next = current->next;
        delete current;
        current = next;
    }
}

LinkedList &LinkedList::operator=(const LinkedList &list)
{
    LinkedList c(list);
    c.swap(*this);
    return *this;
}

LinkedList &LinkedList::operator=(const LinkedList &&list)
{
    LinkedList c(std::move(list));
    c.swap(*this);
    return *this;
}

int LinkedList::first() const
{
    if (mData)
    {
        return mData->value;
    }
    return 0;
}

int LinkedList::last() const
{
    if (mData)
    {
        Node *last = mData;
        while (last->next)
        {
            last = last->next;
        }
        return last->value;
    }
    return 0;
}

void LinkedList::prepend(int value)
{
    Node *newNode = new Node{value, nullptr};
    if (!mData)
    {
        mData = newNode;
    }
    else
    {
        newNode->next = mData;
        mData = newNode;
    }
    mSize++;
}

void LinkedList::append(int value)
{
    Node *newNode = new Node{value, nullptr};
    if (!mData)
    {
        mData = newNode;
    }
    else
    {
        Node *last = mData;
        while (last->next)
        {
            last = last->next;
        }
        last->next = newNode;
    }
    mSize++;
}

bool LinkedList::remove(int value)
{
    Node *curr = mData;
    Node *prev = nullptr;
    while (curr)
    {
        if (curr->value == value)
        {
            if (prev)
            {
                prev->next = curr->next;
            }
            else
            {
                mData = curr->next;
            }
            curr->next = nullptr;
            delete curr;
            mSize--;
            return true;
        }
        prev = curr;
        curr = curr->next;
    }
    return false;
}

bool LinkedList::search(int value) const
{
    for (Node *it = mData; it != nullptr; it = it->next)
    {
        if (it->value == value)
        {
            return true;
        }
    }
    return false;
}

size_t LinkedList::size() const
{
    return mSize;
}

void LinkedList::swap(LinkedList &list)
{
    std::swap(mSize, list.mSize);
    std::swap(mData, list.mData);
}
