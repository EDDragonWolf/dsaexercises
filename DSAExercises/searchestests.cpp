#include "searchestests.h"
#include "measurement.h"
#include "searches.h"
#include <cassert>
#include <iostream>

static const int scSize(10000);
static const int scValue(115243);
static const int scIndex(-1);

void SearchesTests::test()
{
    std::vector<int> v;
    v.reserve(scSize);
    for (int i = 0; i < scSize; ++i)
    {
        v.push_back(i);
    }

    MEASUREMENT_START(linear)
    auto linearResult = Searches::linearSearch(v, scValue);
    MEASUREMENT_END(linear, std::chrono::microseconds)
    std::cout << "Linear: Iterations: " << linearResult.second
              << " Time: " << MEASUREMENT(linear) << std::endl;
    assert(scIndex == linearResult.first);

    MEASUREMENT_START(binary)
    auto binaryResult = Searches::binarySearch(v, scValue);
    MEASUREMENT_END(binary, std::chrono::microseconds)
    std::cout << "Binary: Iterations: " << binaryResult.second
              << " Time: " << MEASUREMENT(binary) << std::endl;
    assert(scIndex == binaryResult.first);

    MEASUREMENT_START(goldenRatio)
    auto goldenRatioResult = Searches::goldenRatioSearch(v, scValue);
    MEASUREMENT_END(goldenRatio, std::chrono::microseconds)
    std::cout << "GoldenRatio: Iterations: " << goldenRatioResult.second
              << " Time: " << MEASUREMENT(goldenRatio) << std::endl;
    assert(scIndex == goldenRatioResult.first);

    MEASUREMENT_START(ternary)
    auto ternaryResult = Searches::ternarySearch(v, scValue);
    MEASUREMENT_END(ternary, std::chrono::microseconds)
    std::cout << "Ternary: Iterations: " << ternaryResult.second
              << " Time: " << MEASUREMENT(ternary) << std::endl;
    assert(scIndex == ternaryResult.first);

    MEASUREMENT_START(jump)
    auto jumpResult = Searches::jumpSearch(v, scValue);
    MEASUREMENT_END(jump, std::chrono::microseconds)
    std::cout << "Jump: Iterations: " << jumpResult.second
              << " Time: " << MEASUREMENT(jump) << std::endl;
    assert(scIndex == jumpResult.first);

    MEASUREMENT_START(interpolation)
    auto interpolationResult = Searches::interpolationSearch(v, scValue);
    MEASUREMENT_END(interpolation, std::chrono::microseconds)
    std::cout << "Interpolation: Iterations: " << interpolationResult.second
              << " Time: " << MEASUREMENT(interpolation) << std::endl;
    assert(scIndex == interpolationResult.first);
}
