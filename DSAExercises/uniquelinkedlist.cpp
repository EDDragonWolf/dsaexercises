#include "uniquelinkedlist.h"

UniqueLinkedList::UniqueLinkedList() : mData{nullptr}, mSize{0}
{
}

UniqueLinkedList::UniqueLinkedList(const UniqueLinkedList &list)
{
    mSize = list.mSize;
    mData = nullptr;
    Node *current = nullptr;
    for (const Node *it = list.mData.get(); it != nullptr; it = it->next.get())
    {
        if (!mData)
        {
            mData = std::make_unique<Node>(it->value);
            current = mData.get();
        }
        else
        {
            current->next = std::make_unique<Node>(it->value);
            current = current->next.get();
        }
    }
}

UniqueLinkedList::~UniqueLinkedList()
{
    while (mData)
    {
        mData = std::move(mData->next);
    }
}

UniqueLinkedList &UniqueLinkedList::operator=(const UniqueLinkedList &list)
{
    UniqueLinkedList c(list);
    c.swap(*this);
    return *this;
}

UniqueLinkedList &UniqueLinkedList::operator=(const UniqueLinkedList &&list)
{
    UniqueLinkedList c(std::move(list));
    c.swap(*this);
    return *this;
}

void UniqueLinkedList::prepend(int value)
{
    std::unique_ptr<Node> newNode = std::make_unique<Node>(value);
    if (!mData)
    {
        mData = std::move(newNode);
    }
    else
    {
        newNode->next = std::move(mData);
        mData = std::move(newNode);
    }
    mSize++;
}

void UniqueLinkedList::append(int value)
{
    std::unique_ptr<Node> newNode = std::make_unique<Node>(value);
    if (!mData)
    {
        mData = std::move(newNode);
    }
    else
    {
        Node *last = mData.get();
        while (last->next)
        {
            last = last->next.get();
        }
        last->next = std::move(newNode);
    }
    mSize++;
}

void UniqueLinkedList::remove(int value)
{
    Node *curr = mData.get();
    Node *prev = nullptr;
    while (curr)
    {
        if (curr->value == value)
        {
            if (prev)
            {
                prev->next.release();
                prev->next = std::move(curr->next);
            }
            else
            {
                mData.release();
                mData = std::move(curr->next);
            }
            curr->next.release();
            delete curr;
            mSize--;
            break;
        }
        prev = curr;
        curr = curr->next.get();
    }
}

bool UniqueLinkedList::search(int value) const
{
    for (const Node *it = mData.get(); it != nullptr; it = it->next.get())
    {
        if (it->value == value)
        {
            return true;
        }
    }
    return false;
}

size_t UniqueLinkedList::size() const
{
    return mSize;
}

void UniqueLinkedList::swap(UniqueLinkedList &list)
{
    std::swap(mSize, list.mSize);
    std::swap(mData, list.mData);
}
