#ifndef UNIQUELINKEDLIST_H
#define UNIQUELINKEDLIST_H

#include <memory>

class UniqueLinkedList
{
public:
    UniqueLinkedList();
    UniqueLinkedList(const UniqueLinkedList &list);
    UniqueLinkedList(UniqueLinkedList &&list) = default;
    ~UniqueLinkedList();

    UniqueLinkedList &operator=(const UniqueLinkedList &list);
    UniqueLinkedList &operator=(const UniqueLinkedList &&list);

    void prepend(int value);
    void append(int value);
    void remove(int value);

    bool search(int value) const;

    size_t size() const;

    friend void swap(UniqueLinkedList &list1, UniqueLinkedList &list2)
    {
        list1.swap(list2);
    }
    void swap(UniqueLinkedList &list);

private:
    struct Node
    {
        int value = 0;
        std::unique_ptr<Node> next;

        Node(int v) : value{v}, next{nullptr} {}
    };

    std::unique_ptr<Node> mData;
    size_t mSize;
};

#endif // UNIQUELINKEDLIST_H
