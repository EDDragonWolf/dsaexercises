#ifndef MEASUREMENT_EXTENDED_H
#define MEASUREMENT_EXTENDED_H

#include <algorithm>
#include <cassert>
#include <numeric>
#include <vector>
#include <chrono>
#include <map>

template <class Duration>
class Experiment
{
    using systemClock = std::chrono::time_point<std::chrono::system_clock>;
public:
    explicit Experiment(size_t size = 0)
    {
        if (size > 0)
        {
            mMeasurementTimes.reserve(size);
        }
    }

    inline void startMeasurement(const std::string &key = std::string())
    {
        assert(mMeasurements.find(key) == mMeasurements.end());
        mMeasurements[key] = std::chrono::system_clock::now();
    }

    inline void endMeasurement(const std::string &key = std::string())
    {
        assert(mMeasurements.find(key) != mMeasurements.end());
        auto end = std::chrono::system_clock::now();
        auto duration = std::chrono::duration_cast<Duration>(end - mMeasurements[key]).count();
        mMeasurementTimes.push_back(duration);
        mMeasurements.erase(key);
    }

    inline int64_t min() const
    {
        return *std::min_element(mMeasurementTimes.cbegin(), mMeasurementTimes.cend());
    }

    inline int64_t max() const
    {
        return *std::max_element(mMeasurementTimes.cbegin(), mMeasurementTimes.cend());
    }

    inline double avg() const
    {
        return std::accumulate(mMeasurementTimes.cbegin(),
                               mMeasurementTimes.cend(), 0.0) / mMeasurementTimes.size();
    }

private:
    std::vector<int64_t> mMeasurementTimes;
    std::map<std::string, systemClock> mMeasurements;
};

#endif // MEASUREMENT_EXTENDED_H
