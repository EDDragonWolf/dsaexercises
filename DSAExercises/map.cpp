#include "map.h"

Map::Map() : mRoot(nullptr), mSize(0)
{
}

Map::~Map()
{
    if (mRoot)
    {
        delete mRoot;
    }
}

void Map::insert(int key, int value)
{
    Node *newNode = new Node(key, value);
    if (!mRoot)
    {
        mRoot = newNode;
    }
    else
    {
        Node *current = mRoot;
        while (current)
        {
            if (current->key > key)
            {
                if (current->left)
                {
                    current = current->left;
                }
                else
                {
                    current->left = newNode;
                    break;
                }
            }
            else
            {
                if (current->right)
                {
                    current = current->right;
                }
                else
                {
                    current->right = newNode;
                    break;
                }
            }
        }
    }
    mSize++;
}

void Map::remove(int key)
{
    Node *previous = nullptr;
    Node *current = mRoot;
    while (current)
    {
        if (current->key > key)
        {
            previous = current;
            current = current->left;
        }
        else if (current->key < key)
        {
            previous = current;
            current = current->right;
        }
        else
        {
            Node *toRemove = current;
            if (toRemove->left && toRemove->right)
            {
                if (previous)
                {
                    if (previous->left == toRemove)
                    {
                        previous->left = toRemove->left;
                    }
                    else
                    {
                        previous->right = toRemove->left;
                    }
                }
                else
                {
                    mRoot = toRemove->left;
                }
                Node *curr = toRemove->left;
                while (curr->right)
                {
                    curr = curr->right;
                }
                curr->right = toRemove->right;
                toRemove->left = nullptr;
                toRemove->right = nullptr;
            }
            else if (toRemove->left)
            {
                if (previous)
                {
                    if (previous->left == toRemove)
                    {
                        previous->left = toRemove->left;
                    }
                    else
                    {
                        previous->right = toRemove->left;
                    }
                }
                else
                {
                    mRoot = toRemove->left;
                }
                toRemove->left = nullptr;
            }
            else if (toRemove->right)
            {
                if (previous)
                {
                    if (previous->left == toRemove)
                    {
                        previous->left = toRemove->right;
                    }
                    else
                    {
                        previous->right = toRemove->right;
                    }
                }
                else
                {
                    mRoot = toRemove->right;
                }
                toRemove->right = nullptr;
            }
            else
            {
                if (previous)
                {
                    if (previous->left == toRemove)
                    {
                        previous->left = nullptr;
                    }
                    else
                    {
                        previous->right = nullptr;
                    }
                }
                else
                {
                    mRoot = nullptr;
                }
            }
            delete toRemove;
            mSize--;
            break;
        }
    }
}

bool Map::search(int key) const
{
    Node *current = mRoot;
    while (current)
    {
        if (current->key > key)
        {
            current = current->left;
        }
        else if (current->key < key)
        {
            current = current->right;
        }
        else
        {
            return true;
        }
    }
    return false;
}

size_t Map::size() const
{
    return mSize;
}
