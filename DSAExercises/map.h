#ifndef MAP_H
#define MAP_H

class Map
{
public:
    Map();
    ~Map();

    void insert(int key, int value);
    void remove(int key);
    bool search(int key) const;

    size_t size() const;

private:
    struct Node
    {
        int key = 0;
        int value = 0;
        Node *left = nullptr;
        Node *right = nullptr;

        Node(int key, int value)
        {
            this->key = key;
            this->value = value;
        }

        ~Node()
        {
            if (left)
            {
                delete left;
            }
            if (right)
            {
                delete right;
            }
        }
    };

    Node *mRoot;
    size_t mSize;
};

#endif // MAP_H
