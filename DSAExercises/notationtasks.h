#ifndef NOTATIONTASKS_H
#define NOTATIONTASKS_H

#include <algorithm>
#include <vector>

namespace NotationTasks
{
// Task 1: Swap function for two integers
void swap(int &a, int &b)  // Complexity: O(1)
{
    int t = a;
    a = b;
    b = t;
}

// Task 2: The sum of elements of the square 2D matrix
int sum(const std::vector<std::vector<int>> &matrix) // Complexity: O(N^2)
{
    int s = 0;
    for (size_t i = 0; i < matrix.size(); ++i)
    {
        for (size_t j = 0; j < matrix.size(); ++j)
        {
            s += matrix[i][j];
        }
    }
    return s;
}

// Task 3: Find the maximum element in the sorted vector
int maxValue(const std::vector<int> &sortedVector) // Complexity: O(1)
{
    if (sortedVector[0] > sortedVector[sortedVector.size() - 1])
    {
        return sortedVector[0];
    }
    else
    {
        return sortedVector[sortedVector.size() - 1];
    }
}

// Task 4: Find the minimum element in the unsorted vector
int minValue(const std::vector<int> &unsortedVector) // Complexity: O(N)
{
    int min = unsortedVector[0];
    for (size_t i = 1; i < unsortedVector.size(); ++i)
    {
        if (min > unsortedVector[i])
        {
            min = unsortedVector[i];
        }
    }
    return min;
}

// Task 5: Merge two sorted 1D vectors into a single sorted vector
std::vector<int> mergeVectors(const std::vector<int> &v1, const std::vector<int> &v2) // Complexity: O(N+M)
{
    std::vector<int> res;
    res.reserve(v1.size() + v2.size());
    size_t i = 0;
    size_t j = 0;
    while (i < v1.size() || j < v2.size())
    {
        if (i < v1.size() && j < v2.size())
        {
            if (v1[i] < v2[j])
            {
                res.push_back(v1[i]);
                i++;
            }
            else
            {
                res.push_back(v2[j]);
                j++;
            }
        }
        else if (i < v1.size())
        {
            res.push_back(v1[i]);
            i++;
        }
        else if (j < v2.size())
        {
            res.push_back(v2[j]);
            j++;
        }
    }
    return res;
}

// Task 6: Calculate the sum of two specified elements in the vector
int sumElements(const std::vector<int> &v, size_t i, size_t j) // Complexity: O(1)
{
    if (i >= 0 && i < v.size() && j >= 0 && j < v.size())
    {
        return v[i] + v[j];
    }
    return 0;
}

// Task 7: Find if the specified value present in the vector
bool findElementUnsorted(const std::vector<int> &v, int value) // Complexity: O(N)
{
    for (size_t i = 0; i < v.size(); ++i)
    {
        if (v[i] == value)
        {
            return true;
        }
    }
    return false;
}

// Task 8: Find if the specified value present in the sorted vector
bool findElementSorted(const std::vector<int> &v, int value) // Complexity: O(logN)
{
    int s = 0;
    int e = v.size() - 1;
    while (v.size() > 0 && v[s] <= value && value <= v[e])
    {
        int h = s + (e - s) / 2;
        if (v[h] == value)
        {
            return true;
        }
        else if (value < v[h])
        {
            e = h - 1;
        }
        else
        {
            s = h + 1;
        }
    }
    return false;
}

// Task 9: Find the minimum element in the vector and multiply all elements of the vector on it
void vectorMultiply(std::vector<int> &v) // Complexity: O(N)
{
    int min = minValue(v);
    for (size_t i = 0; i < v.size(); ++i)
    {
        v[i] *= min;
    }
}

// Task 10: Calculate the Euclidean distance of the two vectors with the same length
double distance(const std::vector<int> &v1, const std::vector<int> &v2) // Complexity: O(N)
{
    double d = 0;
    for (size_t i = 0; i < v1.size(); ++i)
    {
        d += std::pow(v1[i] - v2[i], 2);
    }
    return std::sqrt(d);
}

// Task 11: Calculate the sum of integer numbers from 1 to n including
int integersSum(int n) // Complexity: O(1)
{
    return n * (n + 1) / 2;
}

// Task 12: Calculate the sum of all elements above the main diagonal of the square matrix
int diagonalSum(const std::vector<std::vector<int>> &matrix) // Complexity: O(N^2)
{
    int s = 0;
    for (size_t i = 0; i < matrix.size(); ++i)
    {
        for (size_t j = i + 1; j < matrix.size(); ++j)
        {
            s += matrix[i][j];
        }
    }
    return s;
}

// Task 13: Find the maximum element in the square 2D matrix and multiply elements of the main diagonal on it
void matrixMultiply(std::vector<std::vector<int>> &matrix) // Complexity: O(N^2)
{
    double max = matrix[0][0];
    for (size_t i = 0; i < matrix.size(); ++i)
    {
        for (size_t j = 0; j < matrix.size(); ++j)
        {
            if (max < matrix[i][j])
            {
                max = matrix[i][j];
            }
        }
    }
    for (size_t i = 0; i < matrix.size(); ++i)
    {
        matrix[i][i] *= max;
    }
}

}

// O(1)     == 4
// O(logN)  == 1
// O(N)     == 5
// O(NlogN) == 0
// O(N^2)   == 3

#endif // NOTATIONTASKS_H
