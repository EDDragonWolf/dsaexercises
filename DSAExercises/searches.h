#ifndef SEARCHES_H
#define SEARCHES_H

#include <utility>
#include <vector>

namespace Searches {

std::pair<int, int> linearSearch(const std::vector<int> vector, int value);

std::pair<int, int> binarySearch(const std::vector<int> vector, int value);

std::pair<int, int> goldenRatioSearch(const std::vector<int> vector, int value);

std::pair<int, int> ternarySearch(const std::vector<int> vector, int value);

std::pair<int, int> jumpSearch(const std::vector<int> vector, int value);

std::pair<int, int> interpolationSearch(const std::vector<int> vector, int value);

}

#endif // SEARCHES_H
