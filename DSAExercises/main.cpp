#include "numbergenerator.h"
#include "notationtests.h"
#include "searchestests.h"
#include "algorithmstasks.h"
#include "tests.h"
#include <iostream>

static const size_t scDataSize(5000);

int main()
{
    AlgorithmsTasks::evaluate(scDataSize);

    SearchesTests::test();

    NotationTests::test();

    std::vector<int> randVector = NumberGenerator::generate(scDataSize);
    Tests::testLinkedList(randVector);
    Tests::testUniqueLinkedList(randVector);
    Tests::testMap(randVector);
    Tests::testHash(randVector);
    Tests::testVectorHash(randVector);
    Tests::testStack(randVector);
    Tests::testQueue(randVector);

    return 0;
}
