/*
 * Copyright 2021 Vitalius Parubochyi, QtTeam
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*!
 * \brief The header file provides a set of the preprocessor macros, which
 * allow quickly measure execution time of some part of code and collect
 * the basic statistic for experiments that contain several measurements.
 *
 * Usage example:
 *
 * #include "measurement.h"
 *
 * // Example 1: Measurement of execution time:
 *
 * // starts execution time measurement with the name \a test
 * MEASUREMENT_START(test);
 *
 * // do something...
 *
 * // ends execution time measurement with the name \a test and calculate execution
 * // time in nanoseconds. You can use other units from std::chrono namespace for other precision.
 * MEASUREMENT_END(test, std::chrono::nanoseconds);
 * std::cout << MEASUREMENT(test) << std::endl; // prints the measured execution time
 *
 * // Example 2: Measurement of execution time for several operations:
 *
 * // starts experiment with the name \a test and the \a size
 * EXPERIMENT(test, size);
 *
 * for (size_t i = 0; i < size; ++i)
 * {
 *     // starts execution time measurement with the name \a test
 *     MEASUREMENT_START(test);
 *
 *     // do something...
 *
 *     // ends execution time measurement with the name \a test and calculate execution time
 *     // in nanoseconds. You can use other units from std::chrono namespace for other precision.
 *     MEASUREMENT_END(test, std::chrono::nanoseconds);
 *     EXPERIMENT_ADD(test, MEASUREMENT(test)); // adds the measurement to the experiment
 * }
 *
 * // prints the minimum, maximum and average value of the measured execution time
 * std::cout << "Min: " << EXPERIMENT_MIN(test)
 *           << "\nMax: " << EXPERIMENT_MAX(test)
 *           << "\nAvg: " << EXPERIMENT_AVG(test) << std::endl;
 */

#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <algorithm>
#include <numeric>
#include <vector>
#include <chrono>

#define ENABLE_MEASUREMENT

#ifdef ENABLE_MEASUREMENT

#define MEASUREMENT_START(x) \
    auto start##x = std::chrono::system_clock::now();

#define MEASUREMENT_END(x, interval) \
    auto end##x = std::chrono::system_clock::now(); \
    auto duration##x = std::chrono::duration_cast<interval>(end##x - start##x).count();

#define MEASUREMENT(x) \
    duration##x

#define EXPERIMENT(x, y) \
    std::vector<int64_t> exp##x; \
    if (y > 0) \
    { \
        exp##x.reserve(y); \
    }

#define EXPERIMENT_ADD(x, y) \
    exp##x.push_back(y);


#define EXPERIMENT_MIN(x) \
    (*std::min_element(exp##x.cbegin(), exp##x.cend()))

#define EXPERIMENT_MAX(x) \
    (*std::max_element(exp##x.cbegin(), exp##x.cend()))

#define EXPERIMENT_AVG(x) \
    std::accumulate(exp##x.cbegin(), exp##x.cend(), 0.0) / exp##x.size()

#else

#define MEASUREMENT_START(x)

#define MEASUREMENT_END(x, interval)

#define MEASUREMENT(x) ""

#define EXPERIMENT(x, y)

#define EXPERIMENT_ADD(x, y)

#define EXPERIMENT_MIN(x)

#define EXPERIMENT_MAX(x)

#define EXPERIMENT_AVG(x)

#endif

#endif // MEASUREMENT_H
