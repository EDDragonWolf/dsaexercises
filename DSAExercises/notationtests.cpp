#include "notationtests.h"
#include "notationtasks.h"
#include <cassert>

void NotationTests::test()
{
    // Task 1
    int a = 5;
    int b = 1;
    NotationTasks::swap(a, b);
    assert(a == 1 && b == 5);

    // Task 2
    const int N = 5;
    std::vector<std::vector<int>> m;
    std::vector<int> row;
    for (int i = 0; i < N; ++i)
    {
        row.push_back(1);
    }
    for (int i = 0; i < N; ++i)
    {
        m.push_back(row);
    }
    assert(N * N == NotationTasks::sum(m));

    // Task 3
    std::vector<int> sv1 = {1, 2, 3, 4, 5, 6, 8};
    std::vector<int> sv2 = {5, 5, 4, 3, 2, 1, -3};
    assert(8 == NotationTasks::maxValue(sv1));
    assert(5 == NotationTasks::maxValue(sv2));

    // Task 4
    std::vector<int> cv3 = {4, 3, 1, 2, 6, -3, 0};
    assert(-3 == NotationTasks::minValue(cv3));

    // Task 5
    int n1 = 7;
    int n2 = 4;
    std::vector<int> v1 = {1, 3, 4, 6, 7, 8, 9};
    std::vector<int> v2 = {0, 2, 5, 6};
    std::vector<int> res = {0, 1, 2, 3, 4, 5, 6, 6, 7, 8, 9};
    std::vector<int> r = NotationTasks::mergeVectors(v1, v2);
    for (int i = 0; i < n1 + n2; ++i)
    {
        assert(r[i] == res[i]);
    }

    // Task 6
    assert(8 == NotationTasks::sumElements(sv1, 2, 4));

    // Task 7
    assert(true == NotationTasks::findElementUnsorted(cv3, 2));
    assert(false == NotationTasks::findElementUnsorted(cv3, -2));

    // Task 8
    assert(true == NotationTasks::findElementUnsorted(sv1, 2));
    assert(false == NotationTasks::findElementUnsorted(sv1, 7));

    // Task 9
    int mN = 7;
    std::vector<int> vm = {2, 3, 4, 6, 7, 8, 9};
    std::vector<int> vr = {4, 6, 8, 12, 14, 16, 18};
    NotationTasks::vectorMultiply(vm);
    for (int i = 0; i < mN; ++i)
    {
        assert(vm[i] == vr[i]);
    }

    // Task 10
    std::vector<int> dv2 = {1, 1, 1, 1, 1, 1, 1, 1, 1};
    std::vector<int> dv1 = {0, 0, 0, 0, 0, 0, 0, 0, 0};
    assert(3 == NotationTasks::distance(dv2, dv1));

    // Task 11
    assert(55 == NotationTasks::integersSum(10));

    // Task 12
    assert((N * (N - 1))/ 2 == NotationTasks::diagonalSum(m));

    //Task 13
    const int N2 = 5;
    std::vector<std::vector<int>> mi;
    std::vector<std::vector<int>> mr;
    std::vector<int> mrow;
    for (int i = 0; i < N2; ++i)
    {
        mrow.push_back(1);
    }
    for (int i = 0; i < N2; ++i)
    {
        mi.push_back(mrow);
        mr.push_back(mrow);
        mr[i][i] = 5;
    }
    mi[0][2] = 5;
    mr[0][2] = 5;
    NotationTasks::matrixMultiply(mi);
    for (int i = 0; i < N2; ++i)
    {
        for (int j = 0; j < N2; ++j)
        {
            assert(mi[i][j] == mr[i][j]);
        }
    }
}
