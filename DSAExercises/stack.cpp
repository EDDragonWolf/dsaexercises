#include "stack.h"

Stack::Stack()
{
}

void Stack::push(int value)
{
    mData.prepend(value);
}

int Stack::pop()
{
    if (mData.size() > 0)
    {
        int v = mData.first();
        mData.remove(v);
        return v;
    }
    return 0;
}

size_t Stack::size() const
{
    return mData.size();
}
