#include "searches.h"
#include <cmath>

std::pair<int, int> Searches::linearSearch(const std::vector<int> vector, int value)
{
    std::pair<int, int> res{-1, 0};
    for (size_t i = 0; i < vector.size(); ++i)
    {
        res.second++;
        if (vector[i] == value)
        {
            res.first = i;
            break;
        }
    }
    return res;
}

std::pair<int, int> Searches::binarySearch(const std::vector<int> vector, int value)
{
    std::pair<int, int> res{-1, 0};
    int s = 0;
    int e = vector.size() - 1;
    while (vector.size() > 0 && vector[s] <= value && value <= vector[e])
    {
        res.second++;
        int h = s + (e - s) / 2;
        if (vector[h] == value)
        {
            res.first = h;
            break;
        }
        else if (value < vector[h])
        {
            e = h - 1;
        }
        else
        {
            s = h + 1;
        }
    }
    return res;
}

std::pair<int, int> Searches::goldenRatioSearch(const std::vector<int> vector, int value)
{
    std::pair<int, int> res{-1, 0};
    int s = 0;
    int e = vector.size() - 1;
    while (vector.size() > 0 && vector[s] <= value && value <= vector[e])
    {
        res.second++;
        int h = s + (e - s) / 1.618;
        if (vector[h] == value)
        {
            res.first = h;
            break;
        }
        else if (value < vector[h])
        {
            e = h - 1;
        }
        else
        {
            s = h + 1;
        }
    }
    return res;
}

std::pair<int, int> Searches::ternarySearch(const std::vector<int> vector, int value)
{
    std::pair<int, int> res{-1, 0};
    int s = 0;
    int e = vector.size() - 1;
    while (vector.size() > 0 && vector[s] <= value && value <= vector[e])
    {
        res.second++;
        int h = s + (e - s) / 3;
        int h2 = e - (e - s) / 3;
        if (vector[h] == value)
        {
            res.first = h;
            break;
        }
        else if (vector[h2] == value)
        {
            res.first = h2;
            break;
        }
        else if (value < vector[h])
        {
            e = h - 1;
        }
        else if (value > vector[h] && value < vector[h2])
        {
            s = h + 1;
            e = h2 - 1;
        }
        else
        {
            s = h2 + 1;
        }
    }
    return res;
}

std::pair<int, int> Searches::jumpSearch(const std::vector<int> vector, int value)
{
    std::pair<int, int> res{-1, 0};
    if (vector.size() > 0 && vector.front() <= value && value <= vector.back())
    {
        int step = std::sqrt(vector.size());
        size_t count = 0;
        size_t current = count * step;
        while (current < vector.size())
        {
            res.second++;
            if (vector[current] == value)
            {
                res.first = count * step;
                break;
            }
            else if (vector[current] < value)
            {
                count++;
                current = count * step;
                continue;
            }
            else
            {
                if (count > 0)
                {
                    for (size_t i = (count - 1) * step; i <= current; ++i)
                    {
                        res.second++;
                        if (vector[i] == value)
                        {
                            res.first = i;
                        }
                    }
                }
                break;
            }
        }
    }
    return res;
}

std::pair<int, int> Searches::interpolationSearch(const std::vector<int> vector, int value)
{
    std::pair<int, int> res{-1, 0};
    int s = 0;
    int e = vector.size() - 1;
    while (vector.size() > 0 && vector[s] <= value && value <= vector[e])
    {
        res.second++;
        int h = s + ((e - s) / (vector[e] - vector[s])) * (value - vector[s]);
        if (vector[h] == value)
        {
            res.first = h;
            break;
        }
        else if (value < vector[h])
        {
            e = h - 1;
        }
        else
        {
            s = h + 1;
        }
    }
    return res;
}
