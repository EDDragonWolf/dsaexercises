#ifndef STACK_H
#define STACK_H

#include "linkedlist.h"

class Stack
{
public:
    Stack();

    void push(int value);
    int pop();

    size_t size() const;

private:
    LinkedList mData;
};

#endif // STACK_H
