#ifndef ALGORITHMSTASKS_H
#define ALGORITHMSTASKS_H

#include <utility>
#include <vector>
#include <random>
#include <iostream>
#include "searches.h"
#include "measurement.h"
#include "numbergenerator.h"

namespace AlgorithmsTasks
{

std::vector<int> merge(const std::vector<int> &v1, const std::vector<int> &v2)
{
    std::vector<int> res;
    res.reserve(v1.size() + v2.size());
    size_t i = 0;
    size_t j = 0;
    while (i < v1.size() || j < v2.size())
    {
        if (i < v1.size() && j < v2.size())
        {
            if (v1[i] < v2[j])
            {
                res.push_back(v1[i]);
                i++;
            }
            else
            {
                res.push_back(v2[j]);
                j++;
            }
        }
        else if (i < v1.size())
        {
            res.push_back(v1[i]);
            i++;
        }
        else if (j < v2.size())
        {
            res.push_back(v2[j]);
            j++;
        }
    }
    return res;
}

std::vector<int> mergeSort(const std::vector<int> &vector, int start = -1, int end = -1)
{
    std::vector<int> res;
    if (start < 0)
    {
        start = 0;
    }
    if (end < 0)
    {
        end = vector.size();
    }
    if (end - start > 1)
    {
        int mid = (start + end) / 2;
        auto left = mergeSort(vector, start, mid);
        auto right = mergeSort(vector, mid, end);
        res = merge(left, right);
    }
    else
    {
        for (int i = start; i < end; ++i)
        {
            res.push_back(vector[i]);
        }
    }
    return res;
}

void printVector(const std::vector<int> &vector)
{
    for (size_t i = 0; i < vector.size(); ++i)
    {
        std::cout << i << " " << vector[i] << std::endl;
    }
    std::cout << std::endl;
}

int linearFind(const std::vector<int> &vector, int value) // Complexity: O(N)
{
    return Searches::linearSearch(vector, value).first;
}

int binaryFind(const std::vector<int> &vector, int value) // Complexity: O(NlogN)
{
    auto sorted = mergeSort(vector);
    return Searches::binarySearch(sorted, value).second;
}

int stdFind(const std::vector<int> &vector, int value) // Complexity: O(NlogN)
{
    std::vector<int> sorted = vector;
    std::sort(sorted.begin(), sorted.end());
    return Searches::binarySearch(sorted, value).second;
}

void evaluate(int size)
{
    std::vector<int> randVector = NumberGenerator::generate(size);
    auto sortedVector = mergeSort(randVector);

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> distr(0, size);

    EXPERIMENT(linear, size / 2)
    EXPERIMENT(binary, size / 2)
    EXPERIMENT(binary2, size / 2)
    EXPERIMENT(stdsort, size / 2)
    for (int i = 0; i < size / 2; ++i)
    {
        int value = randVector[distr(gen)];

        MEASUREMENT_START(lin)
        linearFind(randVector, value);
        MEASUREMENT_END(lin, std::chrono::microseconds)
        EXPERIMENT_ADD(linear, MEASUREMENT(lin))

        MEASUREMENT_START(bin)
        binaryFind(randVector, value);
        MEASUREMENT_END(bin, std::chrono::microseconds)
        EXPERIMENT_ADD(binary, MEASUREMENT(bin))

        MEASUREMENT_START(bin2)
        Searches::binarySearch(randVector, value);
        MEASUREMENT_END(bin2, std::chrono::microseconds)
        EXPERIMENT_ADD(binary2, MEASUREMENT(bin2))

        MEASUREMENT_START(std)
        stdFind(randVector, value);
        MEASUREMENT_END(std, std::chrono::microseconds)
        EXPERIMENT_ADD(stdsort, MEASUREMENT(std))
    }
    std::cout << "Linear AVG time: " << EXPERIMENT_AVG(linear) << std::endl;
    std::cout << "Binary AVG time: " << EXPERIMENT_AVG(binary) << std::endl;
    std::cout << "Binary without sort AVG time: " << EXPERIMENT_AVG(binary2) << std::endl;
    std::cout << "Std AVG time: " << EXPERIMENT_AVG(stdsort) << std::endl;
}

}

#endif // ALGORITHMSTASKS_H
