#include "vectorhash.h"
#include <algorithm>

static const size_t scTableSize(1000);

VectorHash::VectorHash(size_t reserve)
    : mTableSize(reserve == 0 ? scTableSize : reserve)
    , mSize(0)
{
    mData.reserve(mTableSize);
    for (size_t i = 0; i < mTableSize; ++i)
    {
        mData.push_back(HashVector());
    }
}

void VectorHash::insert(int key, int value)
{
    size_t h = hashFunction(key);
    HashData d{h, key, value};
    HashVector &v = mData.at(h);
    if (v.size() == 0)
    {
        v.push_back(d);
        mSize++;
    }
    else
    {
        auto it = std::find_if(v.begin(), v.end(), [key](const HashData &data) {
            return data.key == key;
        });
        if (it != v.end())
        {
            it->value = d.value;
        }
        else
        {
            v.push_back(d);
            mSize++;
        }
    }
}

void VectorHash::remove(int key)
{
    size_t h = hashFunction(key);
    HashVector &v = mData.at(h);
    if (v.size() > 0)
    {
        auto it = std::find_if(v.begin(), v.end(), [key](const HashData &data) {
            return data.key == key;
        });
        if (it != v.end())
        {
            v.erase(it);
            mSize--;
        }
    }
}

bool VectorHash::search(int key) const
{
    size_t h = hashFunction(key);
    const HashVector &v = mData.at(h);
    if (v.size() > 0)
    {
        auto it = std::find_if(v.begin(), v.end(), [key](const HashData &data) {
            return data.key == key;
        });
        return it != v.cend();
    }
    return false;
}

size_t VectorHash::size() const
{
    return mSize;
}

size_t VectorHash::hashFunction(int key) const
{
    return std::abs(key) % mTableSize;
}
