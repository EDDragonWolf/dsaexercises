#ifndef VECTORHASH_H
#define VECTORHASH_H

#include <vector>

class VectorHash
{
public:
    VectorHash(size_t reserve = 0);

    void insert(int key, int value);
    void remove(int key);
    bool search(int key) const;

    size_t size() const;

private:
    struct HashData
    {
        size_t hash;
        int key;
        int value;
    };
    using HashVector = std::vector<HashData>;

    size_t hashFunction(int key) const;

    size_t mTableSize;
    std::vector<HashVector> mData;
    size_t mSize;
};

#endif // VECTORHASH_H
