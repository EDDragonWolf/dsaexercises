#include "tests.h"
#include "measurement.h"
#include "uniquelinkedlist.h"
#include "linkedlist.h"
#include "queue.h"
#include "stack.h"
#include "vectorhash.h"
#include "hash.h"
#include "map.h"
#include <iostream>

void Tests::testLinkedList(const std::vector<int> &data)
{
    std::cout << std::endl << "Linked List Test" << std::endl;
    LinkedList list;

    EXPERIMENT(linked_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        list.append(v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(linked_add) << "; max: "
              << EXPERIMENT_MAX(linked_add) << "; avg: "
              << EXPERIMENT_AVG(linked_add) << ". Size: " << list.size() << std::endl;

    EXPERIMENT(linked_pos_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        list.search(data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_pos_search, MEASUREMENT(search));
    }
    std::cout << "Search pos: min " << EXPERIMENT_MIN(linked_pos_search) << "; max: "
              << EXPERIMENT_MAX(linked_pos_search) << "; avg: "
              << EXPERIMENT_AVG(linked_pos_search) << ". Size: " << list.size() << std::endl;

    EXPERIMENT(linked_neg_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        list.search(-1 * data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_neg_search, MEASUREMENT(search));
    }
    std::cout << "Search neg: min " << EXPERIMENT_MIN(linked_neg_search) << "; max: "
              << EXPERIMENT_MAX(linked_neg_search) << "; avg: "
              << EXPERIMENT_AVG(linked_neg_search) << ". Size: " << list.size() << std::endl;

    EXPERIMENT(linked_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        list.remove(data[rand() % data.size()]);
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(linked_delete) << "; max: "
              << EXPERIMENT_MAX(linked_delete) << "; avg: "
              << EXPERIMENT_AVG(linked_delete) << ". Size: " << list.size() << std::endl;
}

void Tests::testUniqueLinkedList(const std::vector<int> &data)
{
    std::cout << std::endl << "Linked List based on std::unique_ptr Test" << std::endl;
    UniqueLinkedList list;

    EXPERIMENT(linked_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        list.append(v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(linked_add) << "; max: "
              << EXPERIMENT_MAX(linked_add) << "; avg: "
              << EXPERIMENT_AVG(linked_add) << ". Size: " << list.size() << std::endl;

    EXPERIMENT(linked_pos_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        list.search(data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_pos_search, MEASUREMENT(search));
    }
    std::cout << "Search pos: min " << EXPERIMENT_MIN(linked_pos_search) << "; max: "
              << EXPERIMENT_MAX(linked_pos_search) << "; avg: "
              << EXPERIMENT_AVG(linked_pos_search) << ". Size: " << list.size() << std::endl;

    EXPERIMENT(linked_neg_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        list.search(-1 * data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_neg_search, MEASUREMENT(search));
    }
    std::cout << "Search neg: min " << EXPERIMENT_MIN(linked_neg_search) << "; max: "
              << EXPERIMENT_MAX(linked_neg_search) << "; avg: "
              << EXPERIMENT_AVG(linked_neg_search) << ". Size: " << list.size() << std::endl;

    EXPERIMENT(linked_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        list.remove(data[rand() % data.size()]);
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(linked_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(linked_delete) << "; max: "
              << EXPERIMENT_MAX(linked_delete) << "; avg: "
              << EXPERIMENT_AVG(linked_delete) << ". Size: " << list.size() << std::endl;
}

void Tests::testMap(const std::vector<int> &data)
{
    std::cout << std::endl << "Map Test" << std::endl;
    Map map;

    EXPERIMENT(map_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        map.insert(v, v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(map_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(map_add) << "; max: "
              << EXPERIMENT_MAX(map_add) << "; avg: "
              << EXPERIMENT_AVG(map_add) << ". Size: " << map.size() << std::endl;

    EXPERIMENT(map_pos_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        map.search(data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(map_pos_search, MEASUREMENT(search));
    }
    std::cout << "Search pos: min " << EXPERIMENT_MIN(map_pos_search) << "; max: "
              << EXPERIMENT_MAX(map_pos_search) << "; avg: "
              << EXPERIMENT_AVG(map_pos_search) << ". Size: " << map.size() << std::endl;

    EXPERIMENT(map_neg_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        map.search(-1 * data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(map_neg_search, MEASUREMENT(search));
    }
    std::cout << "Search neg: min " << EXPERIMENT_MIN(map_neg_search) << "; max: "
              << EXPERIMENT_MAX(map_neg_search) << "; avg: "
              << EXPERIMENT_AVG(map_neg_search) << ". Size: " << map.size() << std::endl;

    EXPERIMENT(map_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        map.remove(data[rand() % data.size()]);
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(map_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(map_delete) << "; max: "
              << EXPERIMENT_MAX(map_delete) << "; avg: "
              << EXPERIMENT_AVG(map_delete) << ". Size: " << map.size() << std::endl;
}

void Tests::testHash(const std::vector<int> &data)
{
    std::cout << std::endl << "Hash Test" << std::endl;
    Hash hash;

    EXPERIMENT(hash_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        hash.insert(v, v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(hash_add) << "; max: "
              << EXPERIMENT_MAX(hash_add) << "; avg: "
              << EXPERIMENT_AVG(hash_add) << ". Size: " << hash.size() << std::endl;

    EXPERIMENT(hash_pos_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        hash.search(data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_pos_search, MEASUREMENT(search));
    }
    std::cout << "Search pos: min " << EXPERIMENT_MIN(hash_pos_search) << "; max: "
              << EXPERIMENT_MAX(hash_pos_search) << "; avg: "
              << EXPERIMENT_AVG(hash_pos_search) << ". Size: " << hash.size() << std::endl;

    EXPERIMENT(hash_neg_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        hash.search(-1 * data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_neg_search, MEASUREMENT(search));
    }
    std::cout << "Search neg: min " << EXPERIMENT_MIN(hash_neg_search) << "; max: "
              << EXPERIMENT_MAX(hash_neg_search) << "; avg: "
              << EXPERIMENT_AVG(hash_neg_search) << ". Size: " << hash.size() << std::endl;

    EXPERIMENT(hash_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        hash.remove(data[rand() % data.size()]);
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(hash_delete) << "; max: "
              << EXPERIMENT_MAX(hash_delete) << "; avg: "
              << EXPERIMENT_AVG(hash_delete) << ". Size: " << hash.size() << std::endl;
}

void Tests::testVectorHash(const std::vector<int> &data)
{
    std::cout << std::endl << "Vector Hash Test" << std::endl;
    VectorHash hash;

    EXPERIMENT(hash_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        hash.insert(v, v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(hash_add) << "; max: "
              << EXPERIMENT_MAX(hash_add) << "; avg: "
              << EXPERIMENT_AVG(hash_add) << ". Size: " << hash.size() << std::endl;

    EXPERIMENT(hash_pos_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        hash.search(data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_pos_search, MEASUREMENT(search));
    }
    std::cout << "Search pos: min " << EXPERIMENT_MIN(hash_pos_search) << "; max: "
              << EXPERIMENT_MAX(hash_pos_search) << "; avg: "
              << EXPERIMENT_AVG(hash_pos_search) << ". Size: " << hash.size() << std::endl;

    EXPERIMENT(hash_neg_search, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(search);
        hash.search(-1 * data[rand() % data.size()]);
        MEASUREMENT_END(search, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_neg_search, MEASUREMENT(search));
    }
    std::cout << "Search neg: min " << EXPERIMENT_MIN(hash_neg_search) << "; max: "
              << EXPERIMENT_MAX(hash_neg_search) << "; avg: "
              << EXPERIMENT_AVG(hash_neg_search) << ". Size: " << hash.size() << std::endl;

    EXPERIMENT(hash_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        hash.remove(data[rand() % data.size()]);
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(hash_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(hash_delete) << "; max: "
              << EXPERIMENT_MAX(hash_delete) << "; avg: "
              << EXPERIMENT_AVG(hash_delete) << ". Size: " << hash.size() << std::endl;
}

void Tests::testStack(const std::vector<int> &data)
{
    std::cout << std::endl << "Stack Test" << std::endl;
    Stack stack;

    EXPERIMENT(stack_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        stack.push(v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(stack_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(stack_add) << "; max: "
              << EXPERIMENT_MAX(stack_add) << "; avg: "
              << EXPERIMENT_AVG(stack_add) << ". Size: " << stack.size() << std::endl;

    EXPERIMENT(stack_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        stack.pop();
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(stack_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(stack_delete) << "; max: "
              << EXPERIMENT_MAX(stack_delete) << "; avg: "
              << EXPERIMENT_AVG(stack_delete) << ". Size: " << stack.size() << std::endl;
}

void Tests::testQueue(const std::vector<int> &data)
{
    std::cout << std::endl << "Queue Test" << std::endl;
    Queue queue;

    EXPERIMENT(queue_add, data.size());
    for (const auto &v : data)
    {
        MEASUREMENT_START(add);
        queue.enqueue(v);
        MEASUREMENT_END(add, std::chrono::nanoseconds);
        EXPERIMENT_ADD(queue_add, MEASUREMENT(add));
    }
    std::cout << "Insertion: min " << EXPERIMENT_MIN(queue_add) << "; max: "
              << EXPERIMENT_MAX(queue_add) << "; avg: "
              << EXPERIMENT_AVG(queue_add) << ". Size: " << queue.size() << std::endl;

    EXPERIMENT(queue_delete, data.size());
    for (size_t i = 0; i < data.size() / 2; ++i)
    {
        MEASUREMENT_START(delete);
        queue.dequeue();
        MEASUREMENT_END(delete, std::chrono::nanoseconds);
        EXPERIMENT_ADD(queue_delete, MEASUREMENT(delete));
    }
    std::cout << "Remove: min " << EXPERIMENT_MIN(queue_delete) << "; max: "
              << EXPERIMENT_MAX(queue_delete) << "; avg: "
              << EXPERIMENT_AVG(queue_delete) << ". Size: " << queue.size() << std::endl;
}
