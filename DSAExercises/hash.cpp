#include "hash.h"
#include <cmath>

static const size_t scTableSize(1000);

Hash::Hash(size_t reserve)
    : mTableSize(reserve == 0 ? scTableSize : reserve)
    , mSize(0)
{
    mData = new HashData*[mTableSize];
    for (size_t i = 0; i < mTableSize; ++i)
    {
        mData[i] = nullptr;
    }
}

Hash::~Hash()
{
    for (size_t i = 0; i < mTableSize; ++i)
    {
        HashData *node = mData[i];
        while (node)
        {
            HashData *next = node->next;
            delete node;
            node = next;
        }
    }
    delete[] mData;
}

void Hash::insert(int key, int value)
{
    size_t h = hashFunction(key);
    HashData *v = mData[h];
    if (!v)
    {
        mData[h] = new HashData{h, key, value, nullptr};
        mSize++;
    }
    else
    {
        while (v->next)
        {
            if (v->key == key)
            {
                break;
            }
            v = v->next;
        }
        if (v->key == key)
        {
            v->value = value;
        }
        else
        {
            v->next = new HashData{h, key, value, nullptr};
            mSize++;
        }
    }
}

void Hash::remove(int key)
{
    size_t h = hashFunction(key);
    HashData *v = mData[h];
    HashData *p = nullptr;
    if (v)
    {
        while (v->next)
        {
            if (v->key == key)
            {
                break;
            }
            p = v;
            v = v->next;
        }
        if (v->key == key)
        {
            if (p)
            {
                p->next = v->next;
            }
            else
            {
                mData[h] = v->next;
            }
            delete v;
            mSize--;
        }
    }
}

bool Hash::search(int key) const
{
    size_t h = hashFunction(key);
    HashData *v = mData[h];
    if (v)
    {
        while (v->next)
        {
            if (v->key == key)
            {
                break;
            }
            v = v->next;
        }
        return v->key == key;
    }
    return false;
}

size_t Hash::size() const
{
    return mSize;
}

size_t Hash::hashFunction(int key) const
{
    return std::abs(key) % mTableSize;
}
