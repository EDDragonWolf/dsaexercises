#ifndef LINKEDLIST_H
#define LINKEDLIST_H

class LinkedList
{
public:
    LinkedList();
    LinkedList(const LinkedList &list);
    LinkedList(LinkedList &&list);
    ~LinkedList();

    LinkedList &operator=(const LinkedList &list);
    LinkedList &operator=(const LinkedList &&list);

    int first() const;
    int last() const;

    void prepend(int value);
    void append(int value);
    bool remove(int value);

    bool search(int value) const;

    size_t size() const;

    friend void swap(LinkedList &list1, LinkedList &list2)
    {
        list1.swap(list2);
    }
    void swap(LinkedList &list);

private:
    struct Node
    {
        int value = 0;
        Node *next = nullptr;
    };

    Node *mData;
    size_t mSize;
};

#endif // LINKEDLIST_H
