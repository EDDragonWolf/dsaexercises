#ifndef HASH_H
#define HASH_H

class Hash
{
public:
    Hash(size_t reserve = 0);
    ~Hash();

    void insert(int key, int value);
    void remove(int key);
    bool search(int key) const;

    size_t size() const;

private:
    struct HashData
    {
        size_t hash;
        int key;
        int value;
        HashData *next = nullptr;
    };

    size_t hashFunction(int key) const;

    size_t mTableSize;
    HashData **mData;
    size_t mSize;
};

#endif // HASH_H
