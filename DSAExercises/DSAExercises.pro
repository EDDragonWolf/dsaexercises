TEMPLATE = app
CONFIG += console c++17
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        hash.cpp \
        linkedlist.cpp \
        main.cpp \
        map.cpp \
        notationtests.cpp \
        queue.cpp \
        searches.cpp \
        searchestests.cpp \
        stack.cpp \
        tests.cpp \
        uniquelinkedlist.cpp \
        vectorhash.cpp

HEADERS += \
    algorithmstasks.h \
    hash.h \
    linkedlist.h \
    map.h \
    measurement.h \
    measurement_extended.h \
    notationtasks.h \
    notationtests.h \
    numbergenerator.h \
    queue.h \
    searches.h \
    searchestests.h \
    stack.h \
    tests.h \
    uniquelinkedlist.h \
    vectorhash.h
