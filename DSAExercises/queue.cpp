#include "queue.h"

Queue::Queue()
{
}

void Queue::enqueue(int value)
{
    mData.append(value);
}

int Queue::dequeue()
{
    if (mData.size() > 0)
    {
        int v = mData.first();
        mData.remove(v);
        return v;
    }
    return 0;
}

size_t Queue::size() const
{
    return mData.size();
}
