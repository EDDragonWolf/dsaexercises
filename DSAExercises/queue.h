#ifndef QUEUE_H
#define QUEUE_H

#include "linkedlist.h"

class Queue
{
public:
    Queue();

    void enqueue(int value);
    int dequeue();

    size_t size() const;

private:
    LinkedList mData;
};

#endif // QUEUE_H
