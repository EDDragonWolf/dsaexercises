#ifndef TESTS_H
#define TESTS_H

#include <vector>

namespace Tests
{
void testLinkedList(const std::vector<int> &data);
void testUniqueLinkedList(const std::vector<int> &data);
void testMap(const std::vector<int> &data);
void testHash(const std::vector<int> &data);
void testVectorHash(const std::vector<int> &data);
void testStack(const std::vector<int> &data);
void testQueue(const std::vector<int> &data);
};

#endif // TESTS_H
